**Overview of Terraform Module Registry in GitLab**

Terraform provides an option to construct configuration into singular modules to make the overall IaC configuration easier to maintain and reduce cognitive load. The module abstracts out the infrastrcuture code typically by resource types in a single provider. For instance, the infrastructure as code being deployed may be scoped to a Network, Database, and Application layer. See Hashicorp documentation below for more details on module design.

The "Terraform Module Registry" group example contains three projects:
* [App Module Project](https://gitlab.com/guided-explorations/iac/terraform-module-registry/app-module)
* [Network Module Project](https://gitlab.com/guided-explorations/iac/terraform-module-registry)
* [Deployment with Modules Project](https://gitlab.com/guided-explorations/iac/terraform-module-registry/deployment-with-modules)
* [Shared Resources](https://gitlab.com/guided-explorations/iac/terraform-module-registry/shared-resources)

The _App_ and _Network_ module contain a generic Terraform configuration that pushes a single module to the projects Terraform Module Registry. The "Deployment with Modules" consumes the modules from each project using GitLab authentication via CI_JOB_TOKEN. 

**Publish**


```
include:
  - project: 'guided-explorations/iac/terraform-module-registry/shared-resources'
    file: 'Terraform-Module-Upload.gitlab-ci.yml'

stages:
  - upload

upload:
  extends: .terraform_module_upload
  variables:
    TERRAFORM_MODULE_NAME: app-module
    TERRAFORM_MODULE_SYSTEM: local
  stage: upload
```

**Import**


```
before_script:
  - echo -e "credentials \"gitlab.com\" {\n  token = \"$CI_JOB_TOKEN\"\n}" > $TF_CLI_CONFIG_FILE

```

```
module "app_module" {
  source = "gitlab.com/guided-explorations/app-module/local"
  version = "0.0.2"
}

```



## Resources:
* [Module Overview](https://www.terraform.io/docs/language/modules/develop/index.html)
* [Module Composition Best Practices](https://www.terraform.io/docs/language/modules/develop/composition.html)
