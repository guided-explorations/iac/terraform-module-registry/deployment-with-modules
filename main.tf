terraform {
  backend "http" {}
}

module "network_module" {
  source = "gitlab.com/guided-explorations/network-module/local"
  version = "0.0.1"
}

module "app_module" {
  source = "gitlab.com/guided-explorations/app-module/local"
  version = "0.0.2"
}

resource "null_resource" "test_resource" {

}

output "hello_network1" {
  value = module.network_module.hello_world
}

output "hello_app" {
  value = module.app_module.hello_world
}
